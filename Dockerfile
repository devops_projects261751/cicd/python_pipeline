FROM python:3.12.1-slim
LABEL maintainer="sbdevopsx"

WORKDIR /app
COPY . /app
COPY requirements.txt /app

RUN pip install --no-cache-dir -r requirements.txt 

EXPOSE 8080
CMD ["python","/app/src/app.py"]