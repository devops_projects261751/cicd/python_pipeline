# Python Pipeline

> Flow of Pipeline -> 
> git -> .gitlab-ci.yml -> python (lint+test) -> build image -> push image

Below is the project structure - 

```
python_pipeline/
│
├── .gitlab-ci.yml
├── README.md
├── .gitignore
│
├── src/
│   └── __init__.py
│   └── app.py
│
└── tests/
    └── __init__.py
    └── test_app.py
```

The `app.py` and `test_app.py` code file can look as follow:

### src/app.py
```python
def squares(x):
    # Perform calculus
    y = x**2
    return y


if __name__ == "__main__":
    print(f"The square of 10 is: {squares(10)}")
```

### tests/test.py
```python
"""
Unit testing of the automatic batch processing application
"""
import unittest
from src.app import squares


class AppTests(unittest.TestCase):
    def test_app(self):
        """Simple Tests"""
        self.assertEqual(squares(10), 100)
        self.assertNotEqual(squares(2), 5)

    def test_errors(self):
        """Check that method fails when parameter type is not numeric"""
        with self.assertRaises(TypeError):
            squares("foo")


def suite():
    _suite = unittest.TestSuite()
    _suite.addTest(AppTests('test_app'))
    _suite.addTest(AppTests('test_errors'))
    return _suite


if __name__ == "__main__":
    runner = unittest.TextTestRunner()
    runner.run(suite())
```
